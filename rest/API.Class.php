<?php

abstract class API
{

    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';

    private $request;

    /**
     * @var $responseData RESTResponse
     */
    protected $responseData = null;

    private $webmasterId = "89722";

    /**
     * Property: language
     */
    protected $language = '';
    /**
     * Property: endpoint
     * The Model requested in the URI. eg: /files
     */
    protected $endpoint = '';

    private $headers = array();

    private $ip = "";

    protected $mail = null;
    /**
     * Property: args
     * Any additional URI components after the endpoint and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     * or /<endpoint>/<arg0>
     */
    protected $args = Array();
    /**
     * Property: file
     * Stores the input of the PUT request
     */
    protected $file = Null;

    protected $timestamp = "";
    /**
     * Constructor: __construct
     * Allow for CORS, assemble and pre-process the data
     */
    public function __construct($request) {
        $today = getdate();
        $this->timestamp = $today['mday'] . "-" . $today['mon'] . "-" . $today['year'] . " " . $today['hours'] . ":" . $today['minutes'] . ":" . $today['seconds'];
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
//        echo json_encode($_REQUEST);die();

        $this->args = explode('/', rtrim($request, '/'));
        $this->endpoint = array_shift($this->args);
        $this->headers = $this->getRequestHeaders();
        $this->ip = $this->getClientIP();

        $this->method = $_SERVER['REQUEST_METHOD'];
//        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
//            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
//                $this->method = 'DELETE';
//            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
//                $this->method = 'PUT';
//            } else {
//                throw new Exception("Unexpected Header");
//            }
//        }

        switch($this->method) {
        case 'DELETE':
        case 'POST':
            $this->request = $this->_cleanInputs($_POST);
            break;
        case 'GET':
            $this->request = $this->_cleanInputs($_GET);
            break;
        case 'PUT':
            $this->request = $this->_cleanInputs($_GET);
            $this->file = file_get_contents("php://input");
            break;
        default:
            $this->_response('Invalid Method', 405);
            break;
        }

    }

    protected function getToken($length) {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
        }

        return $token;
    }
    /**
     * @return string
     */
    public function getWebmasterId()
    {
        return $this->webmasterId;
    }

    public function sentEmail($recipients, $subject, $body){
        if(!is_array($recipients))
            throw new Exception("Enter at least one recipient to send an email.");

        if($this->mail === null){
            $this->mail = new PHPMailer;
            $this->mail->isSMTP();                                      // Set mailer to use SMTP
            $this->mail->Host = '192.169.214.184';  			  		  // Specify main and backup SMTP servers
            $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
            $this->mail->Username = 'no-reply@incometrader.com';        // SMTP username
            $this->mail->Password = 'karachi123';                       // SMTP password
            $this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $this->mail->Port = 587;                                    // TCP port to connect to
            $this->mail->From = 'no-reply@incometrader.com';
            $this->mail->FromName = 'IncomeTrader';
        }
        $this->mail->isHTML(true);                                  // Set email format to HTML
        $this->mail->Subject = $subject;


        $alt_message = $body;

        $this->mail->Body    = $body;
        $this->mail->AltBody = $alt_message;

        foreach($recipients as $recipient_address => $recipient_name){
            $this->mail->addAddress($recipient_address, $recipient_name);
        }
        $this->mail->addAddress("mohsin.mr@gmail.com","Mohsin");     // Add a recipient

        if(!$this->mail->send()) {
            throw new Exception($this->mail->ErrorInfo);
        }
        return;
    }

    public function curlPost($url, $postFields){

        $curl = curl_init( $url);
        curl_setopt( $curl, CURLOPT_POST, true );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        $response = curl_exec( $curl );
        curl_close( $curl );
        return $response;

    }

    public function curlGet($url, $getFields){

        $query_string = http_build_query($getFields, "", "&");
        $url .= "?" . $query_string;
        $curl = curl_init( $url);
        curl_setopt( $curl, CURLOPT_POST, false);
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        $response = curl_exec( $curl );
        curl_close( $curl );
        return $response;

    }
    /**
     * @return array|string
     */
    public function getRequests()
    {
        return $this->request;
    }

    public function getRequestParams($params){
        $response = array();
        foreach($params as $param => $requried){
            if($this->getRequest($param)){
                $response[$param] = $this->getRequest($param);
            } elseif($requried){
                throw new Exception($param . " is required.");
            }
            else{
                $response[$param] = "";
            }
        }
        return $response;
    }

    /**
     * @return array|string
     */
    public function getRequest($param)
    {
        if(!isset($this->request[$param]))
            return false;
        return $this->request[$param];
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }
    public function getHeader($param)
    {
        if(isset($this->headers[$param])){
            return $this->headers[$param];
        }
        return false;
    }

    public function getClientIP() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'CLI';
        return $this->ip = $ipaddress;
    }

    public function getUserAgent() {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    protected function getRequestHeaders() {
        $this->headers = array();
        foreach($_SERVER as $key => $value) {
            if (substr($key, 0, 5) <> 'HTTP_') {
                continue;
            }
            $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $this->headers[$header] = $value;
        }
        return $this->headers;
    }


//    public function processAPI() {
//        if (!method_exists($this, $this->endpoint)) {
//            $this->responseData = new RESTResponse();
//            $this->responseData->setSuccess(false);
//            $this->responseData->setMessage("Unknown error occurred. Please contact administrator.");
//            $this->responseData->setHttpResponseCode(404);
//
//            header("HTTP/1.1 " . $this->responseData->getHttpResponseCode() . " " . $this->_requestStatus($this->responseData->getHttpResponseCode()));
//            return json_encode($this->responseData->toString());
//        }
//
//        $this->{$this->endpoint}($this->args);
//        $this->responseData = new RESTResponse();
//        $this->responseData->setSuccess(true);
//        $this->responseData->setMessage("No content found.");
//
//        header("HTTP/1.1 " . $this->responseData->getHttpResponseCode() . " " . $this->_requestStatus($this->responseData->getHttpResponseCode()));
//        return json_encode($this->responseData->toString());
//
//        if($this->responseData != null){
//
//                header("HTTP/1.1 " . $this->responseData->getHttpResponseCode() . " " . $this->_requestStatus($this->responseData->getHttpResponseCode()));
//                return json_encode($this->responseData->toString());
//        }
//        else{
//            $this->responseData = new RESTResponse();
//            $this->responseData->setMessage("Unknown error occurred. Please contact administrator.");
//            $this->responseData->setHttpResponseCode(500);
//
//            header("HTTP/1.1 " . $this->responseData->getHttpResponseCode() . " " . $this->_requestStatus($this->responseData->getHttpResponseCode()));
//            return json_encode($this->responseData->toString());
//        }
//    }
//
//    /**
//     * @param $data RESTResponse
//     * @param int $status
//     * @return string
//     */
//    private function _response(){//$data, $status = 200) {
//        header("HTTP/1.1 " . $this->responseHTTPCode . " " . $this->_requestStatus($this->responseHTTPCode));
//        return json_encode($this->responseData);
//    }
    public function processAPI() {
        if (method_exists($this, $this->endpoint)) {
            return $this->_response($this->{$this->endpoint}($this->args));
        }
        return $this->_response("No Endpoint: $this->endpoint", 200);
    }
    private function _response($data, $status = 200) {

//        $this->responseData = $data;
        if($this->responseData != null && is_a($this->responseData, 'RESTResponse')){
//            echo "HTTP/1.1 " . $this->responseData->getHttpResponseCode() . " " . $this->_requestStatus($this->responseData->getHttpResponseCode());
//
//            var_dump($this->responseData);
            header("HTTP/1.1 " . $this->responseData->getHttpResponseCode() . " " . $this->_requestStatus($this->responseData->getHttpResponseCode()));
            return json_encode($this->responseData->toString());
        }
        else{
            $this->responseData = new RESTResponse();
            $this->responseData->setMessage("Unknown error occurred. Please contact administrator.");
            $this->responseData->setHttpResponseCode(200);

            header("HTTP/1.1 " . $this->responseData->getHttpResponseCode() . " " . $this->_requestStatus($this->responseData->getHttpResponseCode()));
            return json_encode($this->responseData->toString());
        }
//        header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
//        echo $this->responseData->getMessage();die();
//        return json_encode($data);
    }
    private function _cleanInputs($data) {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }
        return $clean_input;
    }

    private function _requestStatus($code) {
        $status = array(
            200 => 'OK',
            204 => 'No Content',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code])?$status[$code]:$status[500];
    }
}


