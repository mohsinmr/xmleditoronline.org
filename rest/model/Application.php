<?php
/**
 * Created by PhpStorm.
 * User: mohsin
 * Date: 11/4/19
 * Time: 5:10 PM


 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$log_file = getcwd() . "../api_log.txt";

set_exception_handler(function($e) {
    /**
     * @var $e Exception
     */
    error_log($e->getMessage());
    exit('Something weird happened'); //something a user can understand
});

class Application extends Model {
    /**
     * User constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function getApplicationSettings($permission = "ADMIN"){
        $permission_clause = ($permission == "ADMIN") ? " AND `permission_level` = 'ADMIN' " : " AND (`permission_level` = 'ADMIN'  OR `permission_level` = 'SYSTEM') ";
        $sql = "
			SELECT `key`, `value` FROM general_settings WHERE `key` LIKE 'app_%' ".$permission_clause."
		";

        $statement = $this->it_conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $statement->execute();
        $appSettings = array();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            $appSettings[$row['key']] = $row['value'];
        }
        return $appSettings;
    }

    public function getNationalMerchantSettings($permission = "ADMIN"){
        $permission_clause = ($permission == "ADMIN") ? " AND `permission_level` = 'ADMIN' " : " AND (`permission_level` = 'ADMIN'  OR `permission_level` = 'SYSTEM') ";
        $sql = "
			SELECT `key`, `value` FROM general_settings WHERE `key` LIKE 'national_merchant_%' " . $permission_clause . "
		";

        $statement = $this->it_conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $statement->execute();
        $appSettings = array();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            $appSettings[$row['key']] = $row['value'];
        }
        return $appSettings;
    }
}