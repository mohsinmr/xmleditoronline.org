<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/5/19
 * Time: 10:57 PM
 */

class AppSession
{
    /**
     * @var $token string
     */
    private $token;
    /**
     * @var $user_id int
     */
    private $user_id;
    /**
     * @var $full_name string
     */
    private $full_name;
    /**
     * @var $email string
     */
    private $email;
    /**
     * @var $login_time DateTime
     */
    private $login_time;
    /**
     * @var $timeout DateTime
     */
    private $timeout;
    /**
     * @var $user_agent string
     */
    private $user_agent;
    /**
     * @var $ip string
     */
    private $ip;
    /**
     * @var $status string
     */
    private $status;
    /**
     * @var $isValid boolean
     */
    private $isValid;

    /**
     * @var $errorMessage string
     */
    private $errorMessage;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @param string $full_name
     */
    public function setFullName($full_name)
    {
        $this->full_name = $full_name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return DateTime
     */
    public function getLoginTime()
    {
        return $this->login_time;
    }

    /**
     * @param DateTime $login_time
     */
    public function setLoginTime($login_time)
    {
        $this->login_time = $login_time;
    }

    /**
     * @return DateTime
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param DateTime $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    /**
     * @param string $user_agent
     */
    public function setUserAgent($user_agent)
    {
        $this->user_agent = $user_agent;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->isValid;
    }

    /**
     * @param bool $isValid
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    public function toString(){
        return get_object_vars($this);
    }
}