<?php
/**
 * Created by PhpStorm.
 * User: mohsin
 * Date: 11/5/19
 * Time: 10:44 PM
 */

class UserState {
    /**
     * @var $userId int
     */
    private $userId;
    /**
     * @var $fName string
     */
    private $fName;
    /**
     * @var $lName string
     */
    private $lName;
    /**
     * @var $email string
     */
    private $email;
    /**
     * @var $isVerifiedAccount boolean
     */
    private $isVerifiedAccount;
    /**
     * @var $freeAccessLastDate DateTime
     */
    private $freeAccessLastDate;
    /**
     * @var $token string
     */
    private $token;
    /**
     * @var $ip string
     */
    private $ip;
    /**
     * @var $userAgent string
     */
    private $userAgent;
    /**
     * @var $accountTypes ArrayObject
     */
    private $accountTypes;
    /**
     * @var $status string
     */
    private $status;

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getFName()
    {
        return $this->fName;
    }

    /**
     * @param string $fName
     */
    public function setFName($fName)
    {
        $this->fName = $fName;
    }

    /**
     * @return string
     */
    public function getLName()
    {
        return $this->lName;
    }

    /**
     * @param string $lName
     */
    public function setLName($lName)
    {
        $this->lName = $lName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isVerifiedAccount()
    {
        return $this->isVerifiedAccount;
    }

    /**
     * @param bool $isVerifiedAccount
     */
    public function setIsVerifiedAccount($isVerifiedAccount)
    {
        $this->isVerifiedAccount = $isVerifiedAccount;
    }

    /**
     * @return DateTime
     */
    public function getFreeAccessLastDate()
    {
        return $this->freeAccessLastDate;
    }

    /**
     * @param DateTime $freeAccessLastDate
     */
    public function setFreeAccessLastDate($freeAccessLastDate)
    {
        $this->freeAccessLastDate = $freeAccessLastDate;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param string $userAgent
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }

    /**
     * @return ArrayObject
     */
    public function getAccountTypes()
    {
        return $this->accountTypes;
    }

    /**
     * @param ArrayObject $accountTypes
     */
    public function setAccountTypes($accountTypes)
    {
        $this->accountTypes = $accountTypes;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function toString(){
        return get_object_vars($this);
    }

}