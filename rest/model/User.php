<?php
/**
 * Created by PhpStorm.
 * User: mohsin
 * Date: 11/4/19
 * Time: 5:10 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$log_file = getcwd() . "../api_log.txt";

set_exception_handler(function($e) {
    /**
     * @var $e Exception
     */
    error_log($e->getMessage());
    exit('Something weird happened'); //something a user can understand
});

class User extends Model {
    /**
     * User constructor.
     */
    public function __construct() {
        parent::__construct();
    }
    private function createGUID($namespace = '') {
        static $guid = '';
        $uid = uniqid("", true);
        $data = $namespace;
        $data .= $_SERVER['REQUEST_TIME'];
        $data .= $_SERVER['HTTP_USER_AGENT'];
        $data .= $_SERVER['LOCAL_ADDR'];
        $data .= $_SERVER['LOCAL_PORT'];
        $data .= $_SERVER['REMOTE_ADDR'];
        $data .= $_SERVER['REMOTE_PORT'];
        $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
        $guid = '' .
            substr($hash,  0,  8) .
            '-' .
            substr($hash,  8,  4) .
            '-' .
            substr($hash, 12,  4) .
            '-' .
            substr($hash, 16,  4) .
            '-' .
            substr($hash, 20, 12) .
            '';


        $sql = "SELECT * FROM `app_session` WHERE `token` = :token";
        $params = array(
            ':token' => $guid
        );

        $statement = $this->it_conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $statement->execute($params);
        $res = $statement->fetchAll(PDO::FETCH_ASSOC);
        if (sizeof($res) > 0) {
            $this->createGUID($namespace);
        }


        return $guid;
    }

    public function logout($token){
        $app_session_sql = "UPDATE `app_session` 
                            SET `status` = 'inactive'
                            WHERE
                            `token` = :token";


        $statement = $this->it_conn->prepare($app_session_sql);

        $app_session_params = array();
        $app_session_params[':token'] = $token;

        $statement->execute($app_session_params);
    }
    /**
     * @param $token string
     * @param $api APP
     * @return AppSession
     * @throws Exception
     */
    public function validateSession($token, $api){
        $now = new DateTime();
        $dbAppSession = new AppSession();

        if($token == null || $token == false || $token == ""){
            throw new Exception("Invalid request");
        }


        $sql = "SELECT 
                    `token`, `user_id`, `full_name`, `email`, `login_time`, `timeout`, `user_agent`, `ip`, `status` 
                FROM 
                  `app_session` 
                WHERE 
                    `token` = :token AND 
                    `status` = 'active'";

        /**
         * @var $statement PDOStatement
         */
        $statement = $this->it_conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $statement->bindParam(':token', $token, PDO::PARAM_STR);
        $statement->execute();
//
        $res = $statement->fetchAll(PDO::FETCH_ASSOC);
        try{
            if(sizeof($res) == 0)
                throw new Exception("Invalid request");

            $dbAppSession->setToken($res[0]['token']);
            $dbAppSession->setUserId($res[0]['user_id']);
            $dbAppSession->setFullName($res[0]['full_name']);
            $dbAppSession->setEmail($res[0]['email']);
            $dbAppSession->setUserAgent($res[0]['user_agent']);
            $dbAppSession->setIp($res[0]['ip']);
            $dbAppSession->setStatus($res[0]['status']);


            /*
             * Invalid login time string
             */
            $loginTime = DateTime::createFromFormat('Y-m-d H:i:s', $res[0]['login_time']);

            if(!$loginTime)
                throw new Exception("Error in login time field: " . DateTime::getLastErrors()["errors"][0]);

            $dbAppSession->setLoginTime($loginTime);

            /*
             * Invalid timeout time string
             */
            $timeout = DateTime::createFromFormat('Y-m-d H:i:s', $res[0]['timeout']);
            if(!$timeout)
                throw new Exception("Error in timeout field: " . DateTime::getLastErrors()["errors"][0]);

            $dbAppSession->setTimeout($timeout);

            /**
             * Check for session expiration
             */

            if($dbAppSession->getLoginTime() > $now || $dbAppSession->getTimeout() < $now){
//                echo "session expired";die();
                throw new Exception("Session expired, please sign in again!");
            }



            /*
             * Check associated user account status
             */
            if($dbAppSession->getUserId() == null || $dbAppSession->getUserId() == false || $dbAppSession->getUserId() == 0 || $dbAppSession->getUserId() == "")
                throw new Exception("We were unable to find your details. Please contact administrator.");

            $this->findUser($dbAppSession->getUserId(), $api);

            if($api->userState->getStatus() != "ACTIVE")
                throw new Exception("Your account is not activated. Please contact administrator.");

            $accountTypes = $api->userState->getAccountTypes();

            if(!array_key_exists('trading_tools', $accountTypes))
                throw new Exception("Your trading tools subscription is not activated. Please contact administrator.");

            /*
             * Validate IP and user agent
             */
//            if($dbAppSession->getIp() != $api->getIp() || $dbAppSession->getUserAgent() != $api->getUserAgent()){
////                throw new Exception("Session expired due to change in your network IP or device configuration, please sign in again.");
//                $dbAppSession->setIsValid(false);
//                $dbAppSession->setErrorMessage("Session expired due to change in your network IP or device configuration, please sign in again.");
//                return $dbAppSession;
//            }

            $dbAppSession->setIsValid(true);
            return $dbAppSession;

        }catch (Exception $ex){

            if($dbAppSession->getStatus() === "active" && $dbAppSession->getToken() != ""){
                $this->updateAppSession($dbAppSession->getToken(), array(
                    'status' => 'inactive'
                ));
                $dbAppSession->setStatus('inactive');
            }
            $dbAppSession->setIsValid(false);
            $dbAppSession->setErrorMessage($ex->getMessage());
            return $dbAppSession;
        }
    }

    private function updateAppSession($token, $appSession){
        $query = "UPDATE `app_session` SET ";
        $queryColumns = array();
        $params = array();
        foreach($appSession as $column => $value){
            array_push($queryColumns, ' `'.$column.'` = :'.$column);
            $params[':'.$column] = $value;
        }
        $query .= implode(", ", $queryColumns) . " WHERE token = :token_old ";
//        $query .= " (".implode(",",$queryColumns).") VALUE (".implode(",",$queryValues).") WHERE token = :token_old ";
        $params[':token_old'] = $token;

        /**
         * @var $statement PDOStatement
         */
        $statement = $this->it_conn->prepare($query);

        $statement->execute($params);

        return $statement->rowCount();

    }
    public function findUser($userId, $api){
        $sql = "SELECT
				`mm`.user_id, `mm`.f_name, `mm`.l_name, `mm`.username, `mm`.email, `mm`.password, `mm`.is_verified, `mm`.verification_code,
				`mm`.is_anonymous, `mm`.timestamp, (GROUP_CONCAT(`at`.id, CONCAT(CONCAT(':',CONCAT(CONCAT(at.`db_name`,':'),`at`.title)),':',`mat`.status))) AS 'membership', 
				mm.exclusive_member_id, mm.`status`, mm.`free_access_last_date`, mm.`random_user_id` /* , s.`token`, s.`login_time`, s.`timeout`, s.`user_agent`, s.`ip` */
			FROM
				members_main `mm`
			/* LEFT JOIN
				app_session `s`
			ON
				`s`.`user_id` = `mm`.`user_id` */
			LEFT JOIN
				member_account_type `mat`
			ON
				`mm`.user_id = `mat`.user_id
			LEFT JOIN
				account_type `at`
			ON
				`mat`.account_type_id = `at`.id
			WHERE
				`mm`.`user_id` = :user_id
			GROUP BY
				`mm`.user_id;";
        $params = array(
            ':user_id' => $userId
        );

        $statement = $this->it_conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $statement->execute($params);
        $res = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $this->extractUserAndSession($res, $api);
    }
    private function doesEmailExist($email){
        $sql = "SELECT
				`mm`.user_id
			FROM
				members_main `mm`
			WHERE
				`mm`.`email` = :email
			GROUP BY
				`mm`.user_id;";
        $params = array(
            ':email' => $email
        );

        $statement = $this->it_conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $statement->execute($params);
        return ($statement->rowCount() == 0) ? false : true;
    }
    /**
     * @param $res
     * @param $api APP
     * @return UserState
     * @throws Exception
     */
    private function extractUserAndSession($res, $api){
        $data = array();
        if ($res[0]['membership'] != NULL) {

            $memberships = explode(",", $res[0]['membership']);
//                return $memberships;
            foreach ($memberships as $membership) {
                $temp = explode(":", $membership);
                if(isset($temp[3]))
                    if($temp[3] == "ACTIVE")
                        $data['active_account_type'][$temp[1]] = $temp[2];
            }
        }

        $userState = new UserState();
        $token = "";
        if(!isset($res[0]['token'])){
            $token = $this->createGUID("APP-".$res[0]['user_id']);
        }
        else{
            $token = $res[0]['token'];
        }
        $userState->setToken($token);
        $userState->setUserId($res[0]['user_id']);
        $userState->setFName($res[0]['f_name']);
        $userState->setLName($res[0]['l_name']);
        $userState->setEmail($res[0]['email']);
        $userState->setIsVerifiedAccount($res[0]['is_verified']);
        $userState->setFreeAccessLastDate($res[0]['free_access_last_date']);
        $userState->setIsVerifiedAccount($res[0]['is_verified']);
        $userState->setStatus($res[0]['status']);
        $userState->setAccountTypes($data['active_account_type']);
        $userState->setIp($api->getIp());
        $userState->setUserAgent($api->getUserAgent());
        $api->userState = $userState;

        if ($userState->getStatus() != 'ACTIVE') {
            $api->message = "Your account is blocked, please contact us for your assistance.";
            throw new Exception($api->message);
        }


        $appSession = new AppSession();
        $appSession->setToken($token);
        $appSession->setUserId($res[0]['user_id']);
        $appSession->setFullName($res[0]['f_name'] . " " . $res[0]['l_name']);
        $appSession->setEmail($res[0]['email']);
        $appSession->setIp($api->getClientIP());
        $appSession->setUserAgent($api->getUserAgent());
        $appSession->setStatus($res[0]['status']);
        $api->appSession = $appSession;

        return $userState;
    }

    /**
     * @param $email string
     * @param $password string
     * @param $api APP
     * @return mixed
     * @throws Exception
     */
    public function login($email, $password, $api) {

        global $log_file;

//        if (isset($this->request->email, $this->request->password)) {
//            return array(
//                "success" => false,
//                "message" => "Invalid request."
//            );
//        }
//        $email = $this->request["email"];
//        $password = $this->request["password"];


        $sql = "SELECT
				`mm`.user_id, `mm`.f_name, `mm`.l_name, `mm`.username, `mm`.email, `mm`.password, `mm`.is_verified, `mm`.verification_code,
				`mm`.is_anonymous, `mm`.timestamp, (GROUP_CONCAT(`at`.id, CONCAT(CONCAT(':',CONCAT(CONCAT(at.`db_name`,':'),`at`.title)),':',`mat`.status))) AS 'membership', mm.exclusive_member_id, mm.`status`, mm.`free_access_last_date`, mm.`random_user_id`
			FROM
				members_main `mm`
			LEFT JOIN
				member_account_type `mat`
			ON
				`mm`.user_id = `mat`.user_id
			LEFT JOIN
				account_type `at`
			ON
				`mat`.account_type_id = `at`.id
			WHERE
				`mm`.`email` = :email AND
				`mm`.password = :password
			GROUP BY
				`mm`.user_id;";
        $params = array(
            ':email' => $email,
            ':password' => $password
        );

        $statement = $this->it_conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $statement->execute($params);
        $res = $statement->fetchAll(PDO::FETCH_ASSOC);
        if(sizeof($res) == 0)
            throw new Exception("Invalid username or password.");

        $userState = $this->extractUserAndSession($res, $api);
        $this->createAppSession($userState, $api);
//        $app_session_sql = "INSERT INTO `app_session`
//                      (`token`, `user_id`, `full_name`, `email`, `timeout`, `user_agent`, `ip`, `status`)
//                    VALUE
//                      (:token, :user_id, :full_name, :email, NOW() + INTERVAL 1  DAY, :user_agent, :ip, :status)";
//
//
//        $statement = $this->it_conn->prepare($app_session_sql);
//
//        $app_session_params = array();
//        $app_session_params[':token'] = $userState->getToken();
//        $app_session_params[':user_id'] = $userState->getUserId();
//        $app_session_params[':full_name'] = $userState->getFName() . " " . $userState->getLName();
//        $app_session_params[':email'] = $userState->getEmail();
//        $app_session_params[':ip'] = $api->getIp();
////            $app_session_params[':timeout'] = $_SERVER['HTTP_USER_AGENT'];
//        $app_session_params[':user_agent'] = $api->getUserAgent();
//        $app_session_params[':status'] = 'active';
//
        $statement->execute($app_session_params);

        return $userState->toString();
    }

    private function validateCCLuhnAlgorithm($credit_card_number){
        // Get the first digit
        $firstnumber = substr($credit_card_number, 0, 1);
        // Make sure it is the correct amount of digits. Account for dashes being present.
        switch ($firstnumber)
        {
            case 3:
                if (!preg_match('/^3\d{3}[ \-]?\d{6}[ \-]?\d{5}$/', $credit_card_number))
                {
                    return array(false, 'This is not a valid American Express card number');
                }
                break;
            case 4:
                if (!preg_match('/^4\d{3}[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $credit_card_number))
                {
                    return array(false, 'This is not a valid Visa card number');
                }
                break;
            case 5:
                if (!preg_match('/^5\d{3}[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $credit_card_number))
                {
                    return array(false, 'This is not a valid MasterCard card number');
                }
                break;
            case 6:
                if (!preg_match('/^6011[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $credit_card_number))
                {
                    return array(false, 'This is not a valid Discover card number');
                }
                break;
            default:
                return array(false, 'This is not a valid credit card number');
        }
        // Here's where we use the Luhn Algorithm
        $credit_card_number = str_replace('-', '', $credit_card_number);
        $map = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
            0, 2, 4, 6, 8, 1, 3, 5, 7, 9);
        $sum = 0;
        $last = strlen($credit_card_number) - 1;
        for ($i = 0; $i <= $last; $i++)
        {
            $sum += $map[$credit_card_number[$last - $i] + ($i & 1) * 10];
        }
        if ($sum % 10 != 0)
        {
            return array(false, 'This is not a valid credit card number');
        }
        // If we made it this far the credit card number is in a valid format
        return array(true, 'This is a valid credit card number');
    }

    /**
     * @param $fields UserSignUpObject
     * @param $api APP
     * @throws Exception
     * @return boolean
     */
    public function validateSignUpRequest($fields, $api){
        $expiration_date = explode("/",$fields->getCcExpiry());
        $expiration_date = DateTime::createFromFormat('d-m-Y', '1-'.$expiration_date[0].'-'.$expiration_date[1]);

        if(!$expiration_date){
            throw new Exception("Invalid expiry date format.");
        }
        if($expiration_date < new DateTime()){
            throw new Exception("Credit card seems to be expired.");
        }

        if($this->doesEmailExist($fields->getEmail())){
            throw new Exception("Email address already exists.");
        }
        $ccValidate = $this->validateCCLuhnAlgorithm($fields->getCcNumber());
        if(!$ccValidate[0]){
            throw new Exception("Invalid credit card number.");
        }
        return true;
    }

    /**
     * @param $fields UserSignUpObject
     * @param $api APP
     * @throws Exception
     * @return UserState
     */
    public function signUp($fields, $api){
        /*
		 * Insert user basic info into the database
		 */
        $query = "
			INSERT INTO members_main (f_name, l_name, email, password, zip, street, phone)
			VALUES (:f_name, :l_name, :email, :password, :zip, :street, :phone)
		";

        $statement = $this->it_conn->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

        $params = array(
            ':f_name'		=> $fields->getFName(),
            ':l_name'		=> $fields->getLName(),
            ':email'		=> $fields->getEmail(),
            ':password'		=> $fields->getPassword(),
            ':zip'			=> $fields->getZip(),
            ':street'		=> $fields->getStreet(),
            ':phone'		=> $fields->getPhone()
        );

        $statement->execute($params);

        $user_id = $this->it_conn->lastInsertId();
        if($user_id == 0 || $user_id == null){
            throw new Exception("We were unable to create your account, please contact us at mike@incometrader.com");
        }

        $query = "SELECT `id` FROM `account_type` WHERE `db_name` = 'trading_tools'";
        $statement = $this->it_conn->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $statement->execute();
        $account_types = $statement->fetch();
        $account_type_id = ($account_types == null || $account_types == 0) ? 1 : $account_types["id"];

        /*
         * Subscribe for appropriate account
         */
        $query = "
			INSERT INTO member_account_type (user_id, account_type_id, status)
			VALUES (:user_id, :account_type_id, 'ACTIVE')
		";

        $statement = $this->it_conn->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

        $params = array(
            ':user_id'				=> $user_id,
            ':account_type_id'		=> $account_type_id
        );

        $statement->execute($params);

        /*
         * Insert credit card info
         */
        $ccType = CreditcardType::getType($fields->getCcNumber());

        $query = "SELECT `id` FROM `credit_card_type` WHERE `short_name` = :short_name";
        $statement = $this->it_conn->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $statement->execute(array(
            ':short_name' => $ccType
        ));
        $cc_type = $statement->fetch();
        $cc_type_id = ($cc_type == null || $cc_type == 0) ? 1 : $cc_type["id"];


        $expiration_date = explode("/",$fields->getCcExpiry());
        $query = "
			INSERT INTO credit_card (cc_no, cvv, expiration_date, user_id, cc_type_id)
			VALUES (:credit_card_number, :cvv_number, :expiration_date, :user_id, :cc_type_id)
		";
        $statement = $this->it_conn->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $params = array(
            ':credit_card_number'	=> $fields->getCcNumber(),
            ':expiration_date'		=> $expiration_date[0].$expiration_date[1],
            ':user_id'			    => $user_id,
            ':cc_type_id'		    => $cc_type_id
        );
        $statement->execute($params);

        $userState = $this->findUser($user_id, $api);
        $this->createAppSession($userState, $api);
//        $app_session_sql = "INSERT INTO `app_session`
//                      (`token`, `user_id`, `full_name`, `email`, `timeout`, `user_agent`, `ip`, `status`)
//                    VALUE
//                      (:token, :user_id, :full_name, :email, NOW() + INTERVAL 1  DAY, :user_agent, :ip, :status)";
//
//
//        $statement = $this->it_conn->prepare($app_session_sql);
//
//        $app_session_params = array();
//        $app_session_params[':token'] = $userState->getToken();
//        $app_session_params[':user_id'] = $userState->getUserId();
//        $app_session_params[':full_name'] = $userState->getFName() . " " . $userState->getLName();
//        $app_session_params[':email'] = $userState->getEmail();
//        $app_session_params[':ip'] = $api->getIp();
////            $app_session_params[':timeout'] = $_SERVER['HTTP_USER_AGENT'];
//        $app_session_params[':user_agent'] = $api->getUserAgent();
//        $app_session_params[':status'] = 'active';
//
//        $statement->execute($app_session_params);


        return $userState;
    }

    /**
     * @param $userState UserState
     * @param $api APP
     * @return mixed
     */
    public function createAppSession($userState, $api){
        $app_session_sql = "INSERT INTO `app_session` 
                      (`token`, `user_id`, `full_name`, `email`, `timeout`, `user_agent`, `ip`, `status`) 
                    VALUE 
                      (:token, :user_id, :full_name, :email, NOW() + INTERVAL 1  YEAR, :user_agent, :ip, :status)";


        $statement = $this->it_conn->prepare($app_session_sql);

        $app_session_params = array();
        $app_session_params[':token'] = $userState->getToken();
        $app_session_params[':user_id'] = $userState->getUserId();
        $app_session_params[':full_name'] = $userState->getFName() . " " . $userState->getLName();
        $app_session_params[':email'] = $userState->getEmail();
        $app_session_params[':ip'] = $api->getIp();
//            $app_session_params[':timeout'] = $_SERVER['HTTP_USER_AGENT'];
        $app_session_params[':user_agent'] = $api->getUserAgent();
        $app_session_params[':status'] = 'active';

        return $statement->execute($app_session_params);
    }
}
class UserSignUpObject{
    private $fName = 0;
    private $lName = 0;
    private $email = 0;
    private $phone = 0;
    private $street = 0;
    private $zip = 0;
    private $city = 0;
    private $state = 0;
    private $password = 0;
    private $ccNumber = 0;
    private $ccExpiry = 0;

    public function isValidObject(){
        return $this->fName * $this->lName * $this->email * $this->phone * $this->street * $this->zip * $this->city * $this->state * $this->password * $this->ccNumber * $this->ccExpiry;
    }

    /**
     * @return mixed
     */
    public function getFName()
    {
        return $this->fName;
    }

    /**
     * @param mixed $fName
     */
    public function setFName($fName)
    {
        $this->fName = $fName;
    }

    /**
     * @return mixed
     */
    public function getLName()
    {
        return $this->lName;
    }

    /**
     * @param mixed $lName
     */
    public function setLName($lName)
    {
        $this->lName = $lName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getCcNumber()
    {
        return $this->ccNumber;
    }

    /**
     * @param mixed $ccNumber
     */
    public function setCcNumber($ccNumber)
    {
        $this->ccNumber = $ccNumber;
    }

    /**
     * @return mixed
     */
    public function getCcExpiry()
    {
        return $this->ccExpiry;
    }

    /**
     * @param mixed $ccExpiry
     */
    public function setCcExpiry($ccExpiry)
    {
        $this->ccExpiry = $ccExpiry;
    }
}