<?php
/**
 * Created by PhpStorm.
 * User: mohsin
 * Date: 11/4/19
 * Time: 5:10 PM
 */
$log_file = getcwd() . "../api_log.txt";

//require '../sdb.php';
require_once 'Model.php';

class ContentXML extends Model {
    /**
     * User constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function saveContent($key, $content){
        $sql = "INSERT INTO `content_xml` (`key`, `content`)
                VALUES(:key, :content)
                ON DUPLICATE KEY UPDATE `content` = VALUES (:content);";


        $statement = $this->xeo_conn->prepare($sql);

        $param = array();
        $param[':content'] = $content;
        $param[':key'] = $key;

        $statement->execute($param);
        return $statement->rowCount();
    }
    public function getContent($key){
        $sql = "SELECT `id`, `key`, ``content`, `lastupdate`, `timestamp` FROM `content_xml` WHERE `key` = :key`";
        $statement = $this->xeo_conn->prepare($sql);

        $statement->bindParam(':key', $key, PDO::PARAM_STR);
        $statement->execute();

        return $statement->fetch(PDO::FETCH_ASSOC);
    }
    public function isKeyExists($key){
        $sql = "SELECT COUNT(`id`) AS 'count' FROM `content_xml` WHERE `key` = :key`";
        $statement = $this->xeo_conn->prepare($sql);

        $statement->bindParam(':key', $key, PDO::PARAM_STR);
        $statement->execute();

        $row = $statement->fetch(PDO::FETCH_ASSOC);
        return ($row["count"] == 1);
    }
    public function removeContent($key){
        $sql = "DELETE FROM `content_xml` WHERE `key` = :key";

        $statement = $this->xeo_conn->prepare($sql);

        $param = array();
        $param[':key'] = $key;

        $statement->execute($param);
        return $statement->rowCount();
    }

}