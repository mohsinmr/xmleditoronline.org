<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
define("BASEPATH",__FILE__);
require_once 'app-api.php';
// Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
    $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}

register_shutdown_function(function () {
    $err = error_get_last();
    if (! is_null($err)) {
//        echo json_encode($err);
//        echo json_encode(array(
//            'success' => false,
//            'message' => 'SYSTEM ERROR: '.$err['message'],
//            'payload' => array(),
//            'redirect' => ""
//        ));
    }
});
try {
//    sleep(2);
    $API = new APP($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
    echo $API->processAPI();
} catch (Exception $e) {
    echo json_encode(Array('error' => $e->getMessage()));
}
