<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12/23/19
 * Time: 3:02 PM
 */

class SnapQuote{
    public $symbol            = "";
    public $long_name         = "";
    public $exchange          = "";
    public $exchange_symbol   = "";
    public $is_open           = "";
    public $price_data        = array(
            'open'              => 0,
            'high'              => 0,
            'low'               => 0,
            'last'              => 0,
            'volume'            => 0,
            'change'            => 0,
            'change_percent'    => 0
    );

    /**
     * SnapQuote constructor.
     * @param $QMJsonObject
     * @throws Exception
     */
    public function __construct($QMJsonObject) {

        $quotedata = $QMJsonObject;

        $this->symbol            = isset($quotedata->symbolstring) ? $quotedata->symbolstring : "";
        $this->long_name         = isset($quotedata->longname) ? $quotedata->longname : "";
        $this->exchange          = isset($quotedata->exLgName) ? $quotedata->exLgName : "";
        $this->exchange_symbol   = isset($quotedata->exShName) ? $quotedata->exShName : "";
        $this->is_open           = isset($quotedata->isopen) ? (bool) $quotedata->isopen: "";
        $this->price_data        = array(
                'open'              => isset($quotedata->pricedata->open) ? (double)$quotedata->pricedata->open : 0,
                'high'              => isset($quotedata->pricedata->high) ? (double)$quotedata->pricedata->high : 0,
                'low'               => isset($quotedata->pricedata->low) ? (double)$quotedata->pricedata->low : 0,
                'last'              => isset($quotedata->pricedata->last) ? (double)$quotedata->pricedata->last : 0,
                'volume'            => isset($quotedata->pricedata->sharevolume) ? (double)$quotedata->pricedata->sharevolume : 0,
                'change'            => isset($quotedata->pricedata->change) ? (double)$quotedata->pricedata->change : 0,
                'change_percent'    => isset($quotedata->pricedata->changepercent) ? (double)$quotedata->pricedata->changepercent : 0,
        );

    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @return string
     */
    public function getLongName()
    {
        return $this->long_name;
    }

    /**
     * @return string
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * @return string
     */
    public function getExchangeSymbol()
    {
        return $this->exchange_symbol;
    }

    /**
     * @return bool|string
     */
    public function getisOpen()
    {
        return $this->is_open;
    }

    /**
     * @return array
     */
    public function getPriceData()
    {
        return $this->price_data;
    }


}
class Util
{


    public static function getResponse($url, $opt = array()) {
        // init curl object
        $ch = curl_init();

// define options
        $optArray = array(
            CURLOPT_URL => 'http://www.google.com',
            CURLOPT_RETURNTRANSFER => true
        );

// apply those options
        curl_setopt_array($ch, $optArray);

// execute request and get response
        $result = curl_exec($ch);

// also get the error and response code
        $errors = curl_error($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        return $result;
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content  = curl_exec($ch);

        curl_close($ch);

        return $content;
//        // Get cURL resource
//        $curl = curl_init();
//
//        // Set some options - we are passing in a useragent too here
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => $url,
//            CURLOPT_CONNECTTIMEOUT => 180,
//            CURLOPT_TIMEOUT => 1800
//        ));
//
//        $content = curl_exec($curl);
//
//        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//
//
//        if (!$content) {
//            // Close request to clear up some resources
//            curl_close($curl);
//            throw new Exception('cURL Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
//        }
//        if ($httpcode != 200 && $httpcode != "200") {
//            throw new Exception("HTTP '" . $url . "' Error: " . $httpcode);
//        }
//
//        // Close request to clear up some resources
//        curl_close($curl);
//
//        return $content;

    }
}