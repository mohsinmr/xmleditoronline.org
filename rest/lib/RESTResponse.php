<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/6/19
 * Time: 8:21 PM
 */

class RESTResponse{
    /**
     * @var $success boolean
     */
    private $success = false;
    /**
     * @var $message string
     */
    private $message = "";
    /**
     * @var $payload array
     */
    private $payload = array();
    /**
     * @var $redirect string
     */
    private $redirect = "";
    /**
     * @var $httpResponseCode int
     */
    private $httpResponseCode = 200;

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * @param string $redirect
     */
    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * @return int
     */
    public function getHttpResponseCode()
    {
        return $this->httpResponseCode;
    }

    /**
     * @param int $httpResponseCode
     */
    public function setHttpResponseCode($httpResponseCode)
    {
        $this->httpResponseCode = $httpResponseCode;
    }

    public function toString(){
        return array(
            'success' => $this->success,
            'message' => $this->message,
            'payload' => $this->payload,
            'redirect'=> $this->redirect
        );
    }
}