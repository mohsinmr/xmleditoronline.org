<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/4/19
 * Time: 7:19 PM
 */

class Pagination implements Pageable {
    private $pageNo = 1;
    private $noOfRecords = 0;
    private $orderBy = "";
    private $sort = "DESC";
    private $recordsPerPage = 10;
    private $noOfPages = 0;


    /**
     * @return int
     */
    public function getPageNo()
    {
        return $this->pageNo;
    }

    /**
     * @param int $pageNo
     */
    public function setPageNo($pageNo)
    {
        $this->pageNo = (int) $pageNo;
    }

    /**
     * @return int
     */
    public function getNoOfRecords()
    {
        return $this->noOfRecords;
    }

    /**
     * @param int $noOfRecords
     */
    public function setNoOfRecords($noOfRecords)
    {
        $this->noOfRecords = (int) $noOfRecords;
    }

    /**
     * @return string
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param string $orderBy
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getRecordsPerPage()
    {
        return $this->recordsPerPage;
    }

    /**
     * @param int $recordsPerPage
     */
    public function setRecordsPerPage($recordsPerPage)
    {
        $this->recordsPerPage = (int) $recordsPerPage;
    }

    /**
     * @return int
     */
    public function getNoOfPages()
    {
        return $this->noOfPages;
    }

    /**
     * @param int $noOfPages
     */
    public function setNoOfPages($noOfPages)
    {
        $this->noOfPages = (int) $noOfPages;
    }
    public function toString(){
        return get_object_vars($this);
    }
}