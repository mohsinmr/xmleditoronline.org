<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/4/19
 * Time: 7:16 PM
 */

interface Pageable
{
    /**
     * @return int
     */
    public function getPageNo();

    /**
     * @param int $pageNo
     */
    public function setPageNo($pageNo);

    /**
     * @return int
     */
    public function getNoOfRecords();

    /**
     * @param int $noOfRecords
     */
    public function setNoOfRecords($noOfRecords);

    /**
     * @return string
     */
    public function getOrderBy();

    /**
     * @param string $orderBy
     */
    public function setOrderBy($orderBy);

    /**
     * @return string
     */
    public function getSort();

    /**
     * @param string $sort
     */
    public function setSort($sort);

    /**
     * @return int
     */
    public function getRecordsPerPage();

    /**
     * @param int $recordsPerPage
     */
    public function setRecordsPerPage($recordsPerPage);

    /**
     * @return int
     */
    public function getNoOfPages();

    /**
     * @param int $noOfPages
     */
    public function setNoOfPages($noOfPages);


}