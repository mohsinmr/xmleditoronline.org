<?php
ini_set('error_log', getcwd() . "/api_log.txt"); // Logging file path

require_once 'API.Class.php';
require_once 'model/index.php';
$log_file = getcwd() . "/api_log.txt";


error_reporting(E_ERROR);
date_default_timezone_set('America/New_York');
class APP extends API
{

    /**
     * @var $userState UserState
     */
    public $userState;
    /**
     * @var $appSession AppSession
     */
    public $appSession;
    /**
     * @var $message string
     */
    public $message;

    public $contentXml;


    public function __construct($request, $origin) {
        try{
            parent::__construct($request);
            $this->contentXml = new ContentXML();
        } catch (Exception $ex){

        }
    }



    private function getUniqueKey($key){
        $token = $this->getToken(6);
        $newKey = $key."-".$token;
        if(!$this->contentXml->isKeyExists($newKey))
            return $newKey;
        $this->getUniqueKey($key);
    }
    public function save() {
        $applicationSettings = new Application();
        $settings = $applicationSettings->getApplicationSettings();
        if ($this->getRequest('xml') === false && $this->getRequest('key') === false) {
            return array(
                "success" => false,
                "message" => "Invalid request."
            );
        }
        $xmlParam = $this->getRequest('xml');
        $keyParam = $this->getRequest('key');

        $encodedXML = $xmlParam;
        $key = $this->getRequest("key");
        if($key === "") {
            return array(
                "success" => false,
                "message" => "Invalid request."
            );
        }
        $decodedXML = base64_decode($encodedXML);
        if($decodedXML === false){
            return array(
                "success" => false,
                "message" => "Invalid request."
            );
        }
        if($xmlParam !== false){
            if($settings['public_allow_invalid_xml'] === "NO"){
                $xml = simplexml_load_string($decodedXML);
                if (false === $xml) {
                    $errorMessage = "";
                    $errorMessage .= "XML parse error: ";
                    foreach(libxml_get_errors() as $error) {
                        $errorMessage .= "\t". $error->message.". ";
                    }
                    return array(
                        "success" => false,
                        "message" => $errorMessage
                    );
                }

            }
        }
//        $dbKey = "";
//        $dbXML = "";


        /*
         * Only XML was given in parameters
         */
        if($xmlParam !== false && $keyParam === false){
            $key = md5($this->getClientIP());

            /*
             * check if key already exists
             */
            //
            /*
             * If a user is not allowed to save XML file using single IP
             */
            if($settings['public_allow_multiple_save'] == "NO"){
                /*
                 * if key already exists then append the XML into it
                 */
                $dbKey = $key;
            }
            /*
             * User can save multiple XML files using the same IP
             */
            else{
                /*
                 * if key already exists then call getUniqueKey to get the token and
                 */
                $dbKey = $this->getUniqueKey($key);
            }
            $dbXML = $xmlParam;
            $this->contentXml->saveContent($dbKey, $dbXML);
        }
        /*
         * Both XML and key were provided in the parameters
         */
        elseif($xmlParam !== false && $keyParam !== false){
            //update XML
            $dbKey = $keyParam;
            $dbXML = $xmlParam;
            if($dbXML == "" || $dbXML == null){
                $this->contentXml->removeContent($dbKey);
            }
            else{
                $this->contentXml->saveContent($dbKey, $dbXML);
            }
        }
        /*
         * Only key is provided to get the XML from database
         */
        else{
            $dbKey = $keyParam;
            $result = $this->contentXml->getContent($dbKey);
            $dbXML = $result['content'];
        }

        $this->responseData = new RESTResponse();
        $this->responseData->setHttpResponseCode(200);
        $this->responseData->setSuccess(true);

        try{
            $payload = array(
                'key' => $dbKey,
                'content' => $dbXML
            );
            $this->responseData->setPayload($payload);
            $this->responseData->setSuccess(true);
            $this->responseData->setMessage("Request served.");

        } catch (Exception $ex){
            $this->responseData->setHttpResponseCode(200);
            $this->responseData->setSuccess(false);
            $this->responseData->setMessage($ex->getMessage());

        }
        return $this->responseData->toString();

    }

}
